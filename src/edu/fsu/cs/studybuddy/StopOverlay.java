package edu.fsu.cs.studybuddy;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.readystatesoftware.mapviewballoons.BalloonItemizedOverlay;

public class StopOverlay extends BalloonItemizedOverlay<OverlayItem> {

    private final ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();

    public StopOverlay(Drawable defaultMarker, MapView mapView) {
        super(boundCustom(defaultMarker), mapView);
    }

    public static Drawable boundCustom(Drawable drawable) {
        drawable.setBounds(
                -drawable.getIntrinsicWidth()/2,
                0,
                drawable.getIntrinsicWidth()/2,
                drawable.getIntrinsicHeight());
        return drawable;
    }

    @Override
    protected OverlayItem createItem(int i) {
        return mOverlays.get(i);
    }

    @Override
    public int size() {
        return mOverlays.size();
    }

    public void addOverlay(OverlayItem overLay) {
        mOverlays.add(overLay);
        populate();
    }

    @Override
    protected boolean onBalloonTap(int index, OverlayItem item) {
        return true;
    }

    @Override
    public void draw( Canvas c, MapView m, boolean shadow ) {
        super.draw( c, m, false );
    }
}