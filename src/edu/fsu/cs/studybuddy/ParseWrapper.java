package edu.fsu.cs.studybuddy;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class ParseWrapper {
    private static final String TAG = ParseWrapper.class.getSimpleName();

    static public void saveStudySession(ParseObject studySession, final Context context) {

        studySession.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(context,"Post Successful",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context,"Post Failed",Toast.LENGTH_SHORT).show();
                    Log.e(TAG,"save failed: " + e.getMessage());
                }
            }
        });
    }

    static public List<ParseObject> browseStudySession() {
        List<ParseObject> studySessions = new ArrayList<ParseObject>();

        ParseQuery query = new ParseQuery("StudySession");
        try {
            studySessions = query.find();
        } catch (ParseException e) {
            Log.d(TAG,"Error browseStudySession: " + e.getMessage());
        }

        return studySessions;
    }

    static public List<ParseObject> getStudySessions(Context context) {
        List<ParseObject> studySessions = new ArrayList<ParseObject>();

        ParseQuery query = new ParseQuery("StudySession");
        query.whereEqualTo("id", StudySessionInstallation.id(context));

        try {
            studySessions = query.find();
        } catch (ParseException e) {
            Log.d(TAG,"Was not able to fetch studySessions. " + e.getMessage());
        }

        return studySessions;
    };

    static public List<ParseObject> getStudySessions(String course) {
        List<ParseObject> studySessions = new ArrayList<ParseObject>();

        ParseQuery query = new ParseQuery("StudySession");
        query.whereEqualTo("Course", course);

        try {
            studySessions = query.find();
        } catch (ParseException e) {
            Log.d(TAG,"Was not able to fetch studySessions. " + e.getMessage());
        }

        return studySessions;
    }

    static public void updateStudySession(ParseObject newSession, final Context context) {

        newSession.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(context,"Update Successful",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context,"Update Failed",Toast.LENGTH_SHORT).show();
                    Log.e(TAG,"update failed");
                }
            }
        });

    }

    public static void deleteStudySession(ParseObject oldClass, final Context context) {
        oldClass.deleteInBackground(new DeleteCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(context, "Delete Successful",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e(TAG, "delete failed");
                    Toast.makeText(context, "Could not delete study session", Toast.LENGTH_SHORT)
                    .show();
                }
            }
        });
    }
}
