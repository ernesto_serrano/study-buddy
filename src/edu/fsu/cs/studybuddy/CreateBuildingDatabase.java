
package edu.fsu.cs.studybuddy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

public class CreateBuildingDatabase extends SQLiteOpenHelper {
    private final Context context;
    private static final String DATABASE_NAME = "buildingdb";
    static String sTITLE = "title"; // building name
    static String sLAT = "lat"; // latitude
    static String sLON = "lon"; // longitude
    static String sABBR = "abbr"; // building abbreviations (ex. MCH for Carothers Hall)

    public CreateBuildingDatabase(Context context) {
        super(context, DATABASE_NAME, null, 2);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE buildings "
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT, lat REAL, lon REAL);");

        // parses csv file with building info and loads into database
        ContentValues cv = new ContentValues();
        BufferedReader in = new BufferedReader(new InputStreamReader(context.getResources()
                .openRawResource(R.raw.fsubuildinglist)));

        String reader;
        try {
            while ((reader = in.readLine()) != null) {
                String[] RowData = TextUtils.split(reader, ",");
                String theTITLE = RowData[0];
                double theLAT = Double.parseDouble(RowData[1]);
                double theLON = Double.parseDouble(RowData[2]);
                cv.put(sTITLE, theTITLE);
                cv.put(sLAT, theLAT);
                cv.put(sLON, theLON);
                db.insert("buildings", sTITLE, cv);
                theTITLE = RowData[3];
                cv.put(sTITLE, theTITLE);
                db.insert("buildings", sTITLE, cv);
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        android.util.Log.w("buildings", "Upgrading database, which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS buildings");
        onCreate(db);
    }
}