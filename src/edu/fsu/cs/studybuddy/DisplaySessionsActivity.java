package edu.fsu.cs.studybuddy;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseObject;

public class DisplaySessionsActivity extends Activity
{
    private static final String TAG = DisplaySessionsActivity.class.getSimpleName();
    private ListView mStudySessionResult;
    private List<ParseObject> mstudySessions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_sessions);

        mStudySessionResult = (ListView) findViewById(R.id.current_list);

        Uri uri = getIntent().getData();
        try {
            // action is search, get the course we're looking for
            Cursor cursor = managedQuery(uri, null, null, null, null);
            cursor.moveToFirst();
            int courseIndex = cursor.getColumnIndexOrThrow(CreateClassDatabase.COURSE);
            String course = cursor.getString(courseIndex);
            cursor.close();
            new FetchInfo().execute(course);
        } catch (NullPointerException e) {
            // This means that the action is 'browse', proceed with browse action
            new FetchInfo().execute((String) null);
        }

    }

    public void onClick(View view) {
        ActionBarActions.Actions(view);
    }

    public class StudySessionAdapter extends ArrayAdapter<ParseObject> {

        StudySessionAdapter() {
            super(DisplaySessionsActivity.this, R.layout.row_study_session, mstudySessions);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            View session = convertView;

            TextView CourseView;
            TextView DescriptionView;
            TextView DateView;

            LayoutInflater inflater = getLayoutInflater();
            session = inflater.inflate(R.layout.row_study_session, parent, false);

            CourseView = (TextView) session.findViewById(R.id.rowStudySessionCourse);
            DescriptionView = (TextView) session.findViewById(R.id.rowStudySessionDescription);
            DateView = (TextView) session.findViewById(R.id.rowStudySessionDate);

            CourseView.setText(mstudySessions.get(pos).getString("Course"));

            if (mstudySessions.get(pos).getString("Description").trim().compareTo("") != 0) {
                DescriptionView.setText(mstudySessions.get(pos).getString("Description"));
            } else {
                DescriptionView.setVisibility(View.GONE);
            }

            try {
                Date startTime = (Date) mstudySessions.get(pos).get("StartTime");
                DateView.setText(startTime.toLocaleString());
            } catch (NullPointerException e) {
                DateView.setText("");
            }
            return session;
        }
    }

    public class FetchInfo extends AsyncTask<String, Void, List<ParseObject>> {
        ProgressBar mActionBarProgress = (ProgressBar) findViewById(R.id.actionProgress);

        @Override
        protected void onPreExecute() {
            mActionBarProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ParseObject> doInBackground(String... course) {
            switch (course.length) {
                case 0:
                    return ParseWrapper.browseStudySession();
                case 1:
                    return ParseWrapper.getStudySessions(course[0]);
                default:
                    Log.e(TAG,"Unsupported numbers of parameters passed to FetchInfo");
                    return null;
            }
        }

        @Override
        protected void onPostExecute(List<ParseObject> result) {
            mstudySessions = result;
            if (!mstudySessions.isEmpty()) {
                mStudySessionResult.setAdapter(new StudySessionAdapter());
                mStudySessionResult.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                        Intent mapIntent = new Intent(DisplaySessionsActivity.this,
                                StudySessionActivity.class);
                        ParseObjectParcelable parseObjectExtra =
                                new ParseObjectParcelable(mstudySessions.get(arg2));
                        mapIntent.putExtra("StudySession", parseObjectExtra);
                        DisplaySessionsActivity.this.startActivity(mapIntent);
                    }
                });
            } else {
                TextView studySessionLabel = (TextView) findViewById(R.id.current_sessions);
                studySessionLabel.setText("No study sessions found");
            }
            mActionBarProgress.setVisibility(View.INVISIBLE);
        }
    }

}