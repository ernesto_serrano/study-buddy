
package edu.fsu.cs.studybuddy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.ParseObject;

import edu.fsu.cs.studybuddy.CreateClassDatabase.CreateClassDB;

public class PostSessionActivity extends Activity implements TextWatcher {
    private static final String TAG = PostSessionActivity.class.getSimpleName();

    private CreateBuildingDatabase mBuilingDB = null;
    private CreateClassDB mClassDB = null;

    private AutoCompleteTextView mEditClass;
    private AutoCompleteTextView mEditBuilding;
    private Button mDateDisplay;
    private Button mTimeDisplay;
    private Button mPostUpdate;
    private Button mCancel;
    // Ids have to be distinct
    static final int DATE_DIALOG_ID = 0;
    static final int TIME_DIALOG_ID = 1;

    private String mPostBuilding, mPostClass, mPostDetails, mPostDuration;
    private Date mPostStartTime, mPostEndTime;
    boolean isUpdateStudySession;
    SimpleDateFormat formatterDate;
    SimpleDateFormat formatterTime;
    ParseObject oldClass;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_session);

        mPostUpdate = (Button) findViewById(R.id.postButton);
        mEditClass = (AutoCompleteTextView) findViewById(R.id.autoCompletePostClass);
        mEditBuilding = (AutoCompleteTextView) findViewById(R.id.autoCompletePostBuilding);
        mCancel = (Button) findViewById(R.id.postCancel);
        formatterDate = new SimpleDateFormat(getString(R.string.formatterDate));
        formatterTime = new SimpleDateFormat(getString(R.string.formatterTime));
        isUpdateStudySession = getIntent().hasExtra("StudySession") ? true : false;

        // set up the activity
        initAutoCompleteFields();
        populateDurationField();
        initDateTime();
        if (isUpdateStudySession) {
            Button btnUpdate = (Button) findViewById(R.id.postButton);
            btnUpdate.setText("Update");
            mCancel.setText("Delete");
            prePopulateData();
        }

        if (isUpdateStudySession) {
            mCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    setUpCancelBtn();
                }
            });
        } else {
            mCancel.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent goMainScreen = new Intent(PostSessionActivity.this,
                            StudyBuddyActivity.class);
                    startActivity(goMainScreen);
                }
            });
        }

        mPostUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoCompleteTextView errorCheckClass = (AutoCompleteTextView)findViewById
                        (R.id.autoCompletePostClass);
                AutoCompleteTextView errorCheckBuilding = (AutoCompleteTextView)findViewById
                        (R.id.autoCompletePostBuilding);
                mPostClass = errorCheckClass.getText().toString();
                mPostBuilding = errorCheckBuilding.getText().toString();
                mPostDetails = ((EditText) findViewById(R.id.postDetails)).getText().toString();

                // error check for blank fields
                if (mPostClass.isEmpty()) {
                    errorToastMissing("Class");
                    errorCheckClass.requestFocus();
                    return;
                } else if (mPostBuilding.isEmpty()) {
                    errorToastMissing("Building");
                    errorCheckBuilding.requestFocus();
                    return;
                }

                // error check for valid inputs
                if(!inDatabaseClass(mPostClass)) {
                    errorToastInvalid(mPostClass);
                    errorCheckClass.requestFocus();
                    return;
                } else if (!inDatabaseBuilding(mPostBuilding)) {
                    errorToastInvalid(mPostBuilding);
                    errorCheckBuilding.requestFocus();
                    return;
                }

                // formats start and end time based on duration
                formatStartEndTime();

                // Use Parse to post StudySession
                ParseObject studySession = new ParseObject("StudySession");
                studySession.put("Course", mPostClass);
                studySession.put("Section",JSONObject.NULL);
                studySession.put("Building", mPostBuilding);
                studySession.put("Description", mPostDetails);
                studySession.put("StartTime", new java.sql.Timestamp(mPostStartTime.getTime()));
                studySession.put("EndTime", new java.sql.Timestamp(mPostEndTime.getTime()));
                studySession.put("id", StudySessionInstallation.id(getApplicationContext()));
                if (isUpdateStudySession) {
                    studySession.setObjectId(oldClass.getObjectId());
                }

                ParseWrapper.saveStudySession(studySession, getApplicationContext());

                Intent goMainScreen = new Intent(PostSessionActivity.this,
                        StudyBuddyActivity.class);
                startActivity(goMainScreen);
            }
        });
    }

    private void setUpCancelBtn() {
        new AlertDialog.Builder(this)
        .setMessage("Are you sure you want to delete the study session?")
        .setNegativeButton("Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        })
        .setPositiveButton("Delete", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ParseWrapper.deleteStudySession(oldClass, getApplicationContext());

                Intent goMainScreen = new Intent(PostSessionActivity.this,
                        StudyBuddyActivity.class);
                startActivity(goMainScreen);
            }
        })
        .show();

    }

    private void errorToastMissing(String error) {
        Toast errorToast = Toast.makeText(PostSessionActivity.this,
                "All Fields Required! Missing: " + error,Toast.LENGTH_LONG);
        errorToast.setGravity(Gravity.CENTER, 0, 0);
        errorToast.show();
    }

    private void errorToastInvalid(String error) {
        Toast errorToast = Toast.makeText(PostSessionActivity.this,
                error + " is not valid. Please select a value from the suggestions",
                Toast.LENGTH_LONG);
        errorToast.setGravity(Gravity.CENTER, 0, 0);
        errorToast.show();
    }

    private boolean inDatabaseClass(String val) {
        String classQuery =
                "SELECT 1 FROM FTSdictionary WHERE suggest_text_1='" + val + "'";
        Cursor cursor  = mClassDB.getReadableDatabase().rawQuery(classQuery,null);
        boolean exists;
        if (cursor.getCount() > 0) {
            exists = true;
        } else {
            exists = false;
        }
        cursor.close();
        return exists;
    }

    private boolean inDatabaseBuilding(String building) {
        String buildingQuery =
                "SELECT 1 FROM buildings WHERE title='" + mPostBuilding + "'";
        Cursor cursor = mBuilingDB.getReadableDatabase().rawQuery(buildingQuery, null);
        boolean exists;
        if (cursor.getCount() > 0) {
            exists = true;
        } else {
            exists = false;
        }
        cursor.close();
        return exists;
    }

    private void formatStartEndTime() {
        String formatDate = getString(R.string.formatterDate);
        String formatTime = getString(R.string.formatterTime);
        DateFormat formatter = new SimpleDateFormat(formatDate+formatTime);
        Date date;
        Calendar calendar = Calendar.getInstance();

        try {
            date = formatter.parse(mDateDisplay.getText().toString()+mTimeDisplay.getText()
                    .toString());
        } catch (ParseException e) {
            Log.e(TAG, "formatStartEndTime: " + e.getMessage());
            // fill information with current time
            date = calendar.getTime();
        }

        calendar.setTime(date);
        mPostStartTime = date;
        String [] splitDoubles = mPostDuration.split(" ");
        // checks to see if value has half
        Double doubleDuration = Double.parseDouble(splitDoubles[0]);
        Double half = doubleDuration % 1;
        // if half adjust minutes and subtract
        if (half == .5) {
            doubleDuration -= .5;
            calendar.add(Calendar.MINUTE, 30);
        }
        int integerDuration = doubleDuration.intValue();
        // add duration to calculate end time
        calendar.add(Calendar.HOUR_OF_DAY, integerDuration);
        mPostEndTime = calendar.getTime();
    }

    private final DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year,
                int monthOfYear, int dayOfMonth) {
            monthOfYear++;
            String stringTime = String.valueOf(year)+" "+String.valueOf(monthOfYear)+" "+
                    String.valueOf(dayOfMonth);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy M dd");
            try {
                Date time = formatter.parse(stringTime);
                mDateDisplay.setText(formatterDate.format(time));
            } catch (ParseException e) {
                Log.i("PostSessionActivity", "mDateSetListener: " + e.getMessage());
            }
        }
    };

    public final TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            String stringTime = hourOfDay + " " + minute;
            SimpleDateFormat formatter = new SimpleDateFormat("HH mm");
            try {
                Date time = formatter.parse(stringTime);
                mTimeDisplay.setText(formatterTime.format(time));
            } catch (ParseException e) {
                Log.i("PostSessionActivity", "mTimeSetListener: " + e.getMessage());
            }
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        final Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int mHour = cal.get(Calendar.HOUR_OF_DAY);
        int mMinute = cal.get(Calendar.MINUTE);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, year, month, day);
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute, false);
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    private class MyOnItemSelectedListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            mPostDuration = (String) parent.getItemAtPosition(pos);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }

    public void onClick(View view) {
        ActionBarActions.Actions(view);
    }

    public void initDateTime() {
        mDateDisplay = (Button) findViewById(R.id.postDate);
        mTimeDisplay = (Button) findViewById(R.id.postTime);
        Calendar cal = Calendar.getInstance();

        mDateDisplay.setText(formatterDate.format(cal.getTime()));
        mTimeDisplay.setText(formatterTime.format(cal.getTime()));

        mDateDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        mTimeDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(TIME_DIALOG_ID);
            }
        });
    }

    private void initAutoCompleteFields() {
        // TODO: Look to see if some of this code can be moved to a background task
        Cursor mBuilingConstantsCursor;
        Cursor mClassConstantsCursor;
        String[] mbuildingItems;
        String[] mclassItems;
        final int mClassCursorIndex = 0;
        final int mBuildingCursorIndex = 0;

        /******** set up class ********/
        mClassDB = new CreateClassDB(this);
        mClassConstantsCursor = mClassDB.getReadableDatabase().rawQuery(
                "SELECT suggest_text_1 FROM FTSdictionary", null);
        mClassConstantsCursor.moveToFirst();
        mclassItems = new String[mClassConstantsCursor.getCount()];

        if (mClassConstantsCursor.moveToFirst()) {
            for (int classCount = 0; classCount < mClassConstantsCursor.getCount(); classCount++) {
                mclassItems[classCount] = mClassConstantsCursor.getString(mClassCursorIndex);
                mClassConstantsCursor.moveToNext();
            }
        }
        mClassConstantsCursor.close();
        mClassDB.close();

        mEditClass.addTextChangedListener(this);
        mEditClass.setAdapter(new ArrayAdapter<String>(this, R.layout.dropdown_view, mclassItems));

        /******** set up building ********/
        mBuilingDB = new CreateBuildingDatabase(this);
        mBuilingConstantsCursor = mBuilingDB.getReadableDatabase().rawQuery(
                "SELECT title FROM buildings", null);
        mBuilingConstantsCursor.moveToFirst();
        mbuildingItems = new String[mBuilingConstantsCursor.getCount()];

        if (mBuilingConstantsCursor.moveToFirst()) {
            for (int buildingCount = 0; buildingCount < mBuilingConstantsCursor.getCount();
                    buildingCount++) {
                mbuildingItems[buildingCount] = mBuilingConstantsCursor
                        .getString(mBuildingCursorIndex);
                mBuilingConstantsCursor.moveToNext();
            }
        }
        mBuilingConstantsCursor.close();
        mBuilingDB.close();

        mEditBuilding.addTextChangedListener(this);
        mEditBuilding.setAdapter(new ArrayAdapter<String>(this, R.layout.dropdown_view,
                mbuildingItems));
    }

    private void populateDurationField() {
        Spinner durationSpinner = (Spinner) findViewById(R.id.spinnerPostDuration);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.duration_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationSpinner.setAdapter(adapter);

        durationSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
    }

    public void prePopulateData() {
        Date oldDate;
        Intent oldSessionIntent = getIntent();
        EditText mEditDetails = (EditText) findViewById(R.id.postDetails);
        Spinner mEditDuration = (Spinner) findViewById(R.id.spinnerPostDuration);

        ParseObjectParcelable extra =
                (ParseObjectParcelable) oldSessionIntent.getParcelableExtra("StudySession");
        oldClass = extra.getParseObject();
        mEditClass.setText(oldClass.getString("Course"));
        mEditBuilding.setText(oldClass.getString("Building"));

        oldDate = (Date) oldClass.get("StartTime");

        mDateDisplay.setText(formatterDate.format(oldDate));
        mTimeDisplay.setText(formatterTime.format(oldDate));

        // TODO: figure out how to populate spinner with correct value
        mEditDuration.setTag("");
        mEditDetails.setText(oldClass.getString("Description"));
    }
}
