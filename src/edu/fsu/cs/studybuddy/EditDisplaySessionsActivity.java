package edu.fsu.cs.studybuddy;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseObject;

public class EditDisplaySessionsActivity extends Activity{
    //private static final String TAG = EditDisplaySessionsActivity.class.getSimpleName();
    private ListView mStudySessionResult;
    private List<ParseObject> mAvailableStudySessions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_sessions);

        mStudySessionResult = (ListView) findViewById(R.id.current_list);
        new FetchStudySessions().execute();

    }

    public void onClick(View view) {
        ActionBarActions.Actions(view);
    }

    public class StudySessionAdapter extends ArrayAdapter<ParseObject> {

        StudySessionAdapter() {
            super(EditDisplaySessionsActivity.this, R.layout.row_study_session,
                    mAvailableStudySessions);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            View session = convertView;

            TextView CourseView;
            TextView DescriptionView;
            TextView DateView;

            LayoutInflater inflater = getLayoutInflater();
            session = inflater.inflate(R.layout.row_study_session, parent, false);

            CourseView = (TextView) session.findViewById(R.id.rowStudySessionCourse);
            DescriptionView = (TextView) session.findViewById(R.id.rowStudySessionDescription);
            DateView = (TextView) session.findViewById(R.id.rowStudySessionDate);

            CourseView.setText(mAvailableStudySessions.get(pos).getString("Course"));
            DescriptionView.setText(mAvailableStudySessions.get(pos).getString("Description"));
            try {
                Date startTime = (Date) mAvailableStudySessions.get(pos).get("StartTime");
                DateView.setText(startTime.toLocaleString());
            } catch (NullPointerException e) {
                DateView.setText("");
            }
            return session;
        }
    }

    public class FetchStudySessions extends AsyncTask<Void, Void, List<ParseObject>> {
        ProgressBar mActionBarProgress = (ProgressBar) findViewById(R.id.actionProgress);

        @Override
        protected void onPreExecute() {
            mActionBarProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<ParseObject> doInBackground(Void... params) {
            return ParseWrapper.getStudySessions(getApplicationContext());
        }

        @Override
        protected void onPostExecute(List<ParseObject> result) {
            mAvailableStudySessions = result;

            if (mAvailableStudySessions.size() != 0 ) {
                mStudySessionResult.setAdapter(new StudySessionAdapter());
                mStudySessionResult.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                        Intent editIntent = new Intent(EditDisplaySessionsActivity.this,
                                PostSessionActivity.class);
                        ParseObjectParcelable extra = new
                                ParseObjectParcelable(mAvailableStudySessions.get(arg2));
                        editIntent.putExtra("StudySession", extra);

                        startActivity(editIntent);
                    }
                });
            } else {
                TextView noSessionsFound = (TextView) findViewById(R.id.current_sessions);
                noSessionsFound.setText(getString(R.string.no_study_sessions_found));
            }
            mActionBarProgress.setVisibility(View.INVISIBLE);
        }

    }
}