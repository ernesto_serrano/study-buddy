package edu.fsu.cs.studybuddy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.view.View;

public class ActionBarActions {

    static public void Actions(View view) {
        Context context = view.getContext();
        // for haptic feedback on action bar button
        Vibrator mButtonVibation = (Vibrator) view.getContext()
                .getSystemService(Context.VIBRATOR_SERVICE);

        switch (view.getId()) {
            case R.id.action_search:
                mButtonVibation.vibrate(view.getResources().getInteger(R.integer.vibrate_length));
                ((Activity) context).onSearchRequested();
                break;
            case R.id.logoImageActionBar:
                Intent gotoDashboard = new Intent(context,StudyBuddyActivity.class);
                gotoDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(gotoDashboard);
                break;
        }
    }
}
