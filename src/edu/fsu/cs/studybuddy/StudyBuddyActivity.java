package edu.fsu.cs.studybuddy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.parse.Parse;

public class StudyBuddyActivity extends Activity
{
    private static final String TAG = StudyBuddyActivity.class.getSimpleName();
    private Vibrator mButtonVibation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Add Parse
        Parse.initialize(this, "wf3QYQrmQlx3gkvcN9JsYu8AhwThYsDEQeQlATCy",
                "2K6sa30fHmQBmQfjfg0sTOuaDa06QWQtNsbzR1T8");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mButtonVibation = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
    }

    public void onClick(View view) {
        ImageButton menuButton = (ImageButton) view;
        switch (menuButton.getId()) {
            case R.id.menu_search:
                mButtonVibation.vibrate(getResources().getInteger(R.integer.vibrate_length));
                onSearchRequested();
                break;
            case R.id.menu_post:
                mButtonVibation.vibrate(getResources().getInteger(R.integer.vibrate_length));
                startActivity(new Intent(this, PostSessionActivity.class));
                break;
            case R.id.menu_edit_post:
                mButtonVibation.vibrate(getResources().getInteger(R.integer.vibrate_length));
                startActivity(new Intent(this, EditDisplaySessionsActivity.class));
                break;
            case R.id.menu_browse:
                mButtonVibation.vibrate(getResources().getInteger(R.integer.vibrate_length));
                startActivity(new Intent(this, DisplaySessionsActivity.class));
                break;
            default:
                Log.e(TAG,"StudyBuddyActivity: Did not recognize action");
                break;
        }
    }
}