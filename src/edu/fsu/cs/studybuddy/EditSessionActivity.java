
package edu.fsu.cs.studybuddy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.ParseObject;

import edu.fsu.cs.studybuddy.CreateClassDatabase.CreateClassDB;

public class EditSessionActivity extends Activity implements TextWatcher {

    // for spinners
    private AutoCompleteTextView mEditClass;
    private AutoCompleteTextView mEditBuilding;
    private CreateBuildingDatabase mBuilingDB = null;
    private CreateClassDB mClassDB = null;
    private Cursor mBuilingConstantsCursor = null;
    private Cursor mClassConstantsCursor = null;
    private String[] mbuildingItems;
    private String[] mclassItems;
    private final int mClassCursorIndex = 0;
    private final int mBuildingCursorIndex = 0;

    // for date picker
    private int mYear;
    private int mMonth;
    private int mDay;
    private Button mDateDisplay;
    private Button mPickDate;
    static final int DATE_DIALOG_ID = 0;

    // for time picker
    private Button mTimeDisplay;
    private Button mPickTime;
    private int mHour;
    private int mMinute;
    static final int TIME_DIALOG_ID = 1;

    private Spinner mEditDuration;
    private EditText mEditDetails;
    private String mPostBuilding, mPostClass, mPostDetails, mPostDuration;
    private Date mPostStartTime, mPostEndTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_session);

        // set all current values to be edited
        mEditClass = (AutoCompleteTextView) findViewById(R.id.autoCompletePostClass);
        mEditBuilding = (AutoCompleteTextView) findViewById(R.id.autoCompletePostBuilding);
        mDateDisplay = (Button) findViewById (R.id.postDate);
        mTimeDisplay = (Button) findViewById (R.id.postTime);
        mEditDuration = (Spinner) findViewById (R.id.spinnerPostDuration);
        mEditDetails = (EditText) findViewById (R.id.postDetails);

        // pull the class from the server and use the server to populate all fields on the layout
        Intent intent = getIntent();
        final ParseObject oldClass;
        if (intent != null) {
            ParseObjectParcelable extra =
                    (ParseObjectParcelable) intent.getParcelableExtra("StudySession");
            oldClass = extra.getParseObject();
            mEditClass.setText(oldClass.getString("Course"));
            mEditBuilding.setText(oldClass.getString("Building"));
            mDateDisplay.setText("");

            SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:a");
            mTimeDisplay.setText(formatter.format((Date) oldClass.get("StartTime")));

            mEditDuration.setTag("");
            mEditDetails.setText(oldClass.getString("Description"));
        } else {
            oldClass = null;
        }

        // populate spinner for FSU classes
        mClassDB = new CreateClassDB(this);
        mClassConstantsCursor = mClassDB.getReadableDatabase().rawQuery(
                "SELECT suggest_text_1, suggest_text_2 FROM FTSdictionary", null);
        mClassConstantsCursor.moveToFirst();
        mclassItems = new String[mClassConstantsCursor.getCount()];

        if (mClassConstantsCursor.moveToFirst()) {
            for (int classCount = 0; classCount < mClassConstantsCursor.getCount(); classCount++) {
                mclassItems[classCount] = mClassConstantsCursor.getString(mClassCursorIndex);
                mClassConstantsCursor.moveToNext();
            }
        }
        mClassConstantsCursor.close();

        mEditClass.addTextChangedListener(this);
        mEditClass.setAdapter(new ArrayAdapter<String>(this, R.layout.dropdown_view, mclassItems));

        // populate spinner for FSU buildings
        mBuilingDB = new CreateBuildingDatabase(this);
        mBuilingConstantsCursor = mBuilingDB.getReadableDatabase().rawQuery(
                "SELECT title FROM buildings", null);
        mBuilingConstantsCursor.moveToFirst();
        mbuildingItems = new String[mBuilingConstantsCursor.getCount()];

        if (mBuilingConstantsCursor.moveToFirst()) {
            for (int buildingCount = 0; buildingCount < mBuilingConstantsCursor.getCount();
                    buildingCount++) {
                mbuildingItems[buildingCount] = mBuilingConstantsCursor
                        .getString(mBuildingCursorIndex);
                mBuilingConstantsCursor.moveToNext();
            }
        }
        mBuilingConstantsCursor.close();

        mEditBuilding.addTextChangedListener(this);
        mEditBuilding.setAdapter(new ArrayAdapter<String>(this,
                R.layout.dropdown_view, mbuildingItems));

        // populate spinner for duration
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.duration_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mEditDuration.setAdapter(adapter);

        mEditDuration.setOnItemSelectedListener(new MyOnItemSelectedListener());

        // date picker dialog stuff begins here
        mPickDate = (Button) findViewById(R.id.postDate);
        mPickDate.setInputType(0);

        mPickDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showDialog(DATE_DIALOG_ID);
                }
            }
        });

        // get the current date
        final Calendar dateCalendar = Calendar.getInstance();
        mYear = dateCalendar.get(Calendar.YEAR);
        mMonth = dateCalendar.get(Calendar.MONTH);
        mDay = dateCalendar.get(Calendar.DAY_OF_MONTH);

        // display the current date
        updateDateDisplay();

        // time picker stuff starts here
        mPickTime = (Button) findViewById(R.id.postTime);
        mPickTime.setInputType(0);

        // add a click listener to the button
        mPickTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(TIME_DIALOG_ID);
            }
        });

        // get the current time
        final Calendar timeCalendar = Calendar.getInstance();
        mHour = timeCalendar.get(Calendar.HOUR_OF_DAY);
        mMinute = timeCalendar.get(Calendar.MINUTE);

        final Button button = (Button) findViewById(R.id.postButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoCompleteTextView errorCheckClass = (AutoCompleteTextView)findViewById
                        (R.id.autoCompletePostClass);
                mPostClass = errorCheckClass.getText().toString();
                AutoCompleteTextView errorCheckBuilding = (AutoCompleteTextView)findViewById
                        (R.id.autoCompletePostBuilding);
                mPostBuilding = errorCheckBuilding.getText().toString();
                // error checks for blank fields
                if (mPostClass.length() == 0) {
                    errorToast("Class");
                    errorCheckClass.requestFocus();
                    return;
                } else if (mPostBuilding.length() == 0) {
                    errorToast("Building");
                    errorCheckBuilding.requestFocus();
                    return;
                }
                mPostDetails = mEditDetails.getText().toString();

                // formats start and end time based on duration
                formatStartEndTime();

                ParseObject newSession = new ParseObject("StudySession");
                newSession.setObjectId(oldClass.getObjectId());
                newSession.put("Course", mPostClass);
                newSession.put("Section",JSONObject.NULL);
                newSession.put("Building", mPostBuilding);
                newSession.put("Description", mPostDetails);
                newSession.put("StartTime", mPostStartTime);
                newSession.put("EndTime", mPostEndTime);
                newSession.put("id", StudySessionInstallation.id(getApplicationContext()));

                ParseWrapper.updateStudySession(newSession,getApplicationContext());

                Intent goMainScreen =
                        new Intent(EditSessionActivity.this, StudyBuddyActivity.class);
                startActivity(goMainScreen);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void errorToast(String error) {
        Toast.makeText(EditSessionActivity.this, "All Fields Required! Missing: " + error,
                Toast.LENGTH_LONG).show();
    }

    private void formatStartEndTime() {

        DateFormat formatter ;
        Date date = null;

        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            formatter = new SimpleDateFormat("MMM dd, yyyy"+"hh:mm:a");
            date = formatter.parse(mDateDisplay.getText().toString()+mTimeDisplay.getText()
                    .toString());
        } catch (ParseException e) {
            android.util.Log.w("time", "Could not parse dateFormat");
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        mPostStartTime = date;

        // Split double from hour/s
        String [] splitDoubles = mPostDuration.split(" ");
        // checks to see if value has half
        Double doubleDuration = Double.parseDouble(splitDoubles[0]);
        Double half = doubleDuration % 1;
        // if half adjust minutes and subtract
        if (half == .5) {
            doubleDuration -= .5;
            calendar.add(Calendar.MINUTE, 30);
        }
        Integer integerDuration = doubleDuration.intValue();
        // add duration to calculate end time
        calendar.add(Calendar.HOUR_OF_DAY, integerDuration);
        mPostEndTime = calendar.getTime();
    }

    private void updateDateDisplay() {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        Calendar cal = Calendar.getInstance();

        mDateDisplay.setText(formatter.format(cal.getTime()));
    }

    private final DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year,
                int monthOfYear, int dayOfMonth) {
            monthOfYear++;
            String stringTime = String.valueOf(year)+" "+String.valueOf(monthOfYear)+" "+
                    String.valueOf(dayOfMonth);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy M dd");
            try {
                Date time = formatter.parse(stringTime);
                formatter = new SimpleDateFormat("MMM dd, yyyy");
                mDateDisplay.setText(formatter.format(time));
            } catch (ParseException e) {
                Log.i("PostSessionActivity", "updating time did not work");
            }
        }
    };

    public final TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            String stringTime = String.valueOf(hourOfDay)+" "+String.valueOf(minute);
            SimpleDateFormat formatter = new SimpleDateFormat("HH mm");
            try {
                Date time = formatter.parse(stringTime);
                formatter = new SimpleDateFormat("hh:mm:a");
                mTimeDisplay.setText(formatter.format(time));
            } catch (ParseException e) {
                Log.i("PostSessionActivity", "updating time did not work");
            }
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this,
                        mTimeSetListener, mHour, mMinute, false);
        }
        return null;
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    private class MyOnItemSelectedListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent,
                View view, int pos, long id) {
            mPostDuration = (String)parent.getItemAtPosition(pos);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    }

    public void onClick(View view) {
        ActionBarActions.Actions(view);
    }
}