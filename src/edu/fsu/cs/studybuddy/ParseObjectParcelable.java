package edu.fsu.cs.studybuddy;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseObject;

public class ParseObjectParcelable implements Parcelable {
    @SuppressWarnings("unused")
    private static final String TAG = ParseObjectParcelable.class.getSimpleName();

    String mCourse;
    String mBuilding;
    String mDescription;
    Date mStartTime;
    Date mEndTime;
    String mObjectId;
    String mId;

    public ParseObjectParcelable(ParseObject parseObject) {
        mCourse = parseObject.getString("Course");
        mBuilding = parseObject.getString("Building");
        mDescription = parseObject.getString("Description");
        mId = parseObject.getString("id");
        mObjectId = parseObject.getObjectId();
        mStartTime = (Date) parseObject.get("StartTime");
        mEndTime = (Date) parseObject.get("EndTime");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCourse);
        dest.writeString(mBuilding);
        dest.writeString(mDescription);
        dest.writeString(mId);
        dest.writeString(mObjectId);
        dest.writeSerializable(mStartTime);
        dest.writeSerializable(mEndTime);
    }

    public ParseObjectParcelable(Parcel in) {
        mCourse = in.readString();
        mBuilding = in.readString();
        mDescription = in.readString();
        mId = in.readString();
        mObjectId = in.readString();
        mStartTime = (Date) in.readSerializable();
        mEndTime = (Date) in.readSerializable();
    }

    public ParseObject getParseObject() {
        //        Log.d(TAG,"Course :" + mCourse);
        //        Log.d(TAG,"Building :" + mBuilding);
        //        Log.d(TAG,"Des :" + mDescription);
        //        Log.d(TAG,"Id :" + mId);
        //        Log.d(TAG,"ST :" + mStartTime.toLocaleString());
        //        Log.d(TAG,"ET :" + mEndTime.toLocaleString());
        ParseObject parseObject = new ParseObject("StudySession");
        parseObject.setObjectId(mObjectId);
        parseObject.put("Course", mCourse);
        parseObject.put("Building", mBuilding);
        parseObject.put("Description", mDescription);
        parseObject.put("id", mId);
        parseObject.put("StartTime", mStartTime);
        parseObject.put("EndTime", mEndTime);

        return parseObject;
    }

    public static final Parcelable.Creator<ParseObjectParcelable> CREATOR = new
            Parcelable.Creator<ParseObjectParcelable>() {

        @Override
        public ParseObjectParcelable createFromParcel(Parcel in) {
            return new ParseObjectParcelable(in);
        }

        @Override
        public ParseObjectParcelable[] newArray(int size) {
            return new ParseObjectParcelable[size];
        }
    };
}
