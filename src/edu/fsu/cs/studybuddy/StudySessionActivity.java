package edu.fsu.cs.studybuddy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.parse.ParseObject;

public class StudySessionActivity extends MapActivity {
    @SuppressWarnings("unused")
    private static final String TAG = StudySessionActivity.class.getSimpleName();

    private CreateBuildingDatabase mBuildingDatabase = null;
    private Cursor mBuildingDatabaseCursor = null;
    private String mCourse, mDescription, mBuilding;
    private Date mStartTime;
    private Date mEndTime;
    private Double mLatitude;
    private Double mLongitude;
    private Drawable mPin;
    private List<Overlay> mMapOverlays;
    private CustomItemizedOverlay<CustomOverlayItem> mItemizedOverlay;
    private CustomOverlayItem overlayItem;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_session);
        MapView mapView = (MapView) findViewById(R.id.mapview);
        mapView.setBuiltInZoomControls(true);

        // get study session info
        Intent fromDisplay = getIntent();
        ParseObjectParcelable parseObjectExtra = fromDisplay.getParcelableExtra("StudySession");
        ParseObject studySession = parseObjectExtra.getParseObject();
        mCourse = studySession.getString("Course");
        mBuilding = studySession.getString("Building");
        mDescription = studySession.getString("Description");
        mStartTime = (Date) studySession.get("StartTime");
        mEndTime = (Date) studySession.get("EndTime");

        // get the building coordinates
        mBuildingDatabase = new CreateBuildingDatabase(this);
        mBuildingDatabaseCursor = mBuildingDatabase
                .getReadableDatabase()
                .rawQuery("SELECT lat, lon FROM buildings WHERE title = '"+mBuilding+"'", null);
        mBuildingDatabaseCursor.moveToFirst();
        mLatitude = mBuildingDatabaseCursor.getDouble(0);
        mLongitude = mBuildingDatabaseCursor.getDouble(1);
        mBuildingDatabaseCursor.close();
        mBuildingDatabase.close();

        GeoPoint sessionPoint = new GeoPoint((int) (mLatitude * 1E6), (int) (mLongitude * 1E6));
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd 'at' hh:mm a");
        String startTime = dateFormat.format(mStartTime);
        String endTime = dateFormat.format(mEndTime);

        // set up street view
        String coords = mLatitude + "," + mLongitude;
        String mapUrl = getResources().getString(R.string.mapURL);
        String streetImageUrl = String.format(mapUrl, coords, coords);

        // display session info
        String balloonDescription = mCourse + "\n" + mBuilding + "\n" +
                "From: " + startTime + "\n"+
                "To: " + endTime + "\n" +
                mDescription;
        mPin = getResources().getDrawable(R.drawable.balloon);
        mItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(mPin, mapView);
        overlayItem = new CustomOverlayItem(sessionPoint, balloonDescription, " ", streetImageUrl);
        mItemizedOverlay.addOverlay(overlayItem);
        mMapOverlays = mapView.getOverlays();
        mMapOverlays.add(mItemizedOverlay);

        MapController myMapController = mapView.getController();
        myMapController.animateTo(sessionPoint);
        myMapController.setZoom(R.integer.default_zoom);

    }
    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }

    public void onClick(View view) {
        ActionBarActions.Actions(view);
    }
}